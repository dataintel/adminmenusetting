import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BaseWidgetAdminMenuComponent } from './src/base-widget.component'; 

export * from './src/base-widget.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [
    BaseWidgetAdminMenuComponent
  ],
  exports: [
    BaseWidgetAdminMenuComponent
  ]
})
export class AdminMenuSettingModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: AdminMenuSettingModule,
      providers: []
    };
  }
}
